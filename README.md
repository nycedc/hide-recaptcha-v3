# Hide reCAPTCHA v3 Badge

## Description
This is a WordPress plugin that hides Google's reCAPTCHA v3 badge on all pages except those specifically allowed (intended to be those pages actually using reCAPTCHA).

This plugin was developed to work Contact Form 7's reCAPTCHA v3 integration. See this [thread](https://wordpress.org/support/topic/recaptcha-badge-on-all-pages-not-just-pages-with-contact-forms/page/2/) for an interesting discussion on the problem.

### Why not just add this functionality to a site's functions.php file?
Because most WordPress sites are hacky enough as it is! At my job I manage a lot of WordPress sites and the last thing I want to do is add a hard-coded hack to each one, this plugin allows me to safely test each site in an automated fashion.

## Installation
This plugin is meant to be installed via [Composer](https://getcomposer.org/). For details on how Composer can be used with Wordpress see this [documentation](https://wpackagist.org/).
