<?php
/**
 * Plugin Name:     Hide ReCAPTCHA v3
 * Plugin URI:      https://bitbucket.org/nycedc/hide-recaptcha-v3/
 * Description:     Only shows reCAPTCHA v3 badge on a whitelisted group of pages
 * Author:          Andrew Lehman <alehman@edc.nyc>
 * Version:         0.1.0
 */

if (!defined('HR3_STYLE_HANDLE')) {
    define('HR3_STYLE_HANDLE', 'hr3_hide_recaptcha');
}

function activate_hide_recaptcha_v3() {
    add_option('hr3_allowed_pages', '[]');
}

register_activation_hook(__FILE__, 'activate_hide_recaptcha_v3');

function uninstall_hide_recaptcha_v3() {
    delete_option('hr3_allowed_pages');
}

register_uninstall_hook(__FILE__, 'uninstall_hide_recaptcha_v3');

function hr3_render_allowed_pages_field() {
    $allowed_pages = json_decode(get_option('hr3_allowed_pages', '[]'), true);
    ?>
    <textarea name="hr3_allowed_pages" rows="8"><?php echo implode(PHP_EOL, $allowed_pages); ?></textarea>
    <p style="width: 400px;">Each line of this textarea will be treated as a separate pattern to match against page urls.  If a url matches one of these patterns the reCAPTCHA badge will be shown, otherwise it will be hidden.</p>
    <?php
}

function hr3_init() {
	// Create settings and options
	$settings_group = 'hr3_recaptcha';
	$page_slug = 'hide_recaptcha';
	register_setting($settings_group, 'hr3_allowed_pages');

	// Create page section
	$settings_section = 'hr3_main';
	add_settings_section(
		$settings_section,
		'All Settings',
		'',
		$page_slug
	);

	// Add fields to that section
	add_settings_field(
		'allowed_pages',
		'Allowed Pages',
		'hr3_render_allowed_pages_field',
		$page_slug,
		$settings_section
	);
}

add_action('admin_init', 'hr3_init');

function hr3_build_options_page() {
	if (!current_user_can('manage_options')) {
		return;
	}
	?>
	<div class="wrap">
		<h1>Hide ReCAPTCHA v3</h1>
		<form method="post" action="options.php">
			<?php
            settings_fields('hr3_recaptcha');
			do_settings_sections('hide_recaptcha');
			submit_button('Save Pages');
			?>
		</form>
	</div>
	<?php
}

function hr3_create_options_page() {
	add_options_page(
		__('Hide ReCAPTCHA v3'),
		__('Hide ReCAPTCHA v3'),
		'manage_options',
		'hide_recaptcha',
		'hr3_build_options_page'
	);
}

add_action('admin_menu', 'hr3_create_options_page');

function hr3_pre_process_allowed_pages($newValue = null, $oldValue = null, $optionName = '') {
    $sites = preg_split('/\r?\n/', $newValue);

    return json_encode($sites);
}

add_filter('pre_update_option_hr3_allowed_pages', 'hr3_pre_process_allowed_pages');

function hr3_hide_recaptcha() {
    wp_enqueue_style(
        HR3_STYLE_HANDLE,
        plugin_dir_url(__FILE__) . 'css/hide-recaptcha.css'
    );
}

add_action('wp_enqueue_scripts', 'hr3_hide_recaptcha');

function hr3_show_recaptcha() {
    $currentUrl = $_SERVER['REQUEST_URI'];
    $pagesToShow = json_decode(get_option('hr3_allowed_pages', '[]'), true);
    $showRecaptcha = false;

    foreach ($pagesToShow as $page) {
        $regex = preg_quote($page);
        if (preg_match('/' . $regex . '\/?$/', $currentUrl) === 1) {
            $showRecaptcha = true;
            break;
        }
    }


    if ($showRecaptcha) {
        wp_dequeue_style(HR3_STYLE_HANDLE);
    }
}

add_action('wp_enqueue_scripts', 'hr3_show_recaptcha', 20);
